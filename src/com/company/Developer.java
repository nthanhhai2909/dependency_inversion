package com.company;

public class Developer extends Employee {
    public Developer() { }

    @Override
    public void work() {
        System.out.println("Developer starting for work");
    }
}
