package com.company;

public class Main {

    public static void main(String[] args) {
	    Developer developer = new Developer();
	    Tester tester = new Tester();
	    QA qa = new QA();
        Manager mangager = new Manager();
        mangager.addEmployee(developer);
        mangager.addEmployee(tester);
        mangager.addEmployee(qa);
    }
}
