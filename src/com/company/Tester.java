package com.company;

public class Tester extends Employee {
    public Tester() {}

    @Override
    public void work() {
        System.out.println("Tester starting for work");
    }
}
